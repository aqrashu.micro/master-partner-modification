# -*- coding: utf-8 -*-

{
    'name': "Custom Sales & Distribution Module",

    'summary': """
        Custom Sales & Distribution Module
    """,

    'description': """
        Custom Sales & Distribution Module
    """,

    'author': "Hafizh Ibnu Syam",

    'version': '1.0.0',
    'license': 'LGPL-3',

    'depends': [
        'base',
    ],

    # always loaded
    'data': [
        # security
        'security/ir.model.access.csv',
        # data
        'data/brap_distribution_channel_data.xml',
        # views
        'views/sales_distribution_menu_views.xml',
        'views/sales_distribution_views.xml',
    ],
}