# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

_CUSTOMER_TYPE_SELECTION = [
    ('associated', 'Berelasi'),
    ('third party', 'Pihak ke 3'),
]
_PARTNER_TYPE_SELECTION = [
    ('construction', 'Konstruksi'),
    ('consultation', 'Konsultasi'),
]


class Partner(models.Model):
    _inherit = "res.partner"

    hierarchy_level = fields.Integer(
        string='Hierarchy Level',
        compute='_compute_hierarchy_level'
    )
    child_display_name = fields.Char(
        string='Child Hierarchy',
        compute='_compute_child_display_name'
    )
    brap_category_id = fields.Many2one(
        comodel_name='brap.distribution.channel',
        string='Distribution Channel'
    )
    brap_customer_type = fields.Selection(
        selection=_CUSTOMER_TYPE_SELECTION,
        string='Customer Type'
    )
    brap_partner_type = fields.Selection(
        selection=_PARTNER_TYPE_SELECTION,
        string='Partner Type'
    )

    # compute

    def _compute_hierarchy_level(self):
        for rec in self:
            hierarchy_level = 1
            parent_id = rec.parent_id
            while parent_id:
                hierarchy_level += 1
                parent_id = parent_id.parent_id
            rec.hierarchy_level = hierarchy_level

    def _compute_child_display_name(self):
        for rec in self:
            display_name = False
            if rec.child_ids:
                for child in rec.child_ids:
                    if not display_name:
                        display_name = child.name
                    else:
                        display_name += f", {child.name}"
            rec.child_display_name = display_name
