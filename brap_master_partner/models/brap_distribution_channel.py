# -*- coding: utf-8 -*-

from odoo import fields, models


class BrapDistributionChannel(models.Model):
    _name = 'brap.distribution.channel'
    _description = 'Distribution Channel'

    name = fields.Char(
        string='Name'
    )
