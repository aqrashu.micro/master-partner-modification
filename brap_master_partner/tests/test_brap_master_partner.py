# -*- coding: utf-8 -*-

from odoo import SUPERUSER_ID
from odoo.tests.common import TransactionCase
from odoo.tests import tagged


@tagged('standard')
class TestBrapMasterPartner(TransactionCase):
    def setUp(self):
        super(TestBrapMasterPartner, self).setUp()
        self.partner_obj = self.env['res.partner']
        self.distribution_channel_obj = self.env['brap.distribution.channel']
        self.partner_a = self.partner_obj.create({'name': 'Partner A'})
        self.partner_list = [self.partner_a.id]

    def test_check_partner_a(self):
        # check if name is A and hierarki level is 1
        self.assertEqual(self.partner_a.name, 'Partner A')
        self.assertEqual(self.partner_a.hierarchy_level, 1)

    def test_create_new_child_partner_a(self):
        # hierarchy level 2
        partners = [
            {'name': '1'},
            {'name': '2'}
        ]

        for val in partners:
            val['parent_id'] = self.partner_a.id
            val['name'] = self.partner_a.name + val.get('name')

        partners = self.partner_obj.create(partners)

        self.partner_list.extend(partners.ids)

        expected_partners = [
            {'name': 'Partner A1'},
            {'name': 'Partner A2'}
        ]

        expected_names = [partner['name'] for partner in partners]
        actual_names = partners.mapped('name')
        actual_level = partners.mapped('hierarchy_level')

        # test check if name and level is as expected
        self.assertEqual(expected_names, actual_names)
        self.assertEqual(actual_level, [2, 2])

        # test check if number of child and total created is as expected
        self.assertEqual(len(self.partner_a.child_ids), 2)
        self.assertEqual(len(self.partner_list), 3)

        # hierarchy level 3
        vals = []
        for child in self.partner_a.child_ids:
            partners = [
                {'name': '.1'},
                {'name': '.2'},
                {'name': '.3'}
            ]
            for val in partners:
                val['name'] = child.name + val.get('name')
                val['parent_id'] = child.id
                vals.append(val)

        partners = self.partner_obj.create(vals)

        self.partner_list.extend(partners.ids)

        expected_partners = [
            {'name': 'Partner A1.1'},
            {'name': 'Partner A1.2'},
            {'name': 'Partner A1.3'},
            {'name': 'Partner A2.1'},
            {'name': 'Partner A2.2'},
            {'name': 'Partner A2.3'}
        ]

        expected_names = [partner['name'] for partner in partners]
        actual_names = partners.mapped('name')
        actual_level = partners.mapped('hierarchy_level')

        # test check if name and level is as expected
        self.assertEqual(expected_names, actual_names)
        self.assertEqual(actual_level, [3, 3, 3, 3, 3, 3])

        # test check if number of child and total created is as expected
        self.assertEqual(len(self.partner_a.child_ids.child_ids), 6)
        self.assertEqual(len(self.partner_list), 9)

    def test_check_partner_channel_distribution(self):
        # create new distribution channel
        val = {
            'name': 'Channel A'
        }

        distribution_channel = self.distribution_channel_obj.create(val)

        all_distribution_channel = self.distribution_channel_obj.search([])

        # test check if distribution channel name is as expected
        self.assertEqual(distribution_channel.name, val.get('name'))
        # test check if total distribution channel is as expected
        self.assertEqual(len(all_distribution_channel), 3)

        # create new partner using newly created channel
        val = {
            'name': 'Partner Channel A',
            'brap_category_id': distribution_channel.id,
        }
        partner = self.partner_obj.create(val)

        # test check if partner channel name is as expected
        self.assertEqual(partner.name, val.get('name'))
        # test check if partner distribution channel is as expected
        self.assertEqual(partner.brap_category_id.id, val.get('brap_category_id'))
